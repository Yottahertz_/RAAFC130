/*-------------------------------------------------------------------
very simple JATO script for Sabre[Dust]s C130

based on my very simple JATO script
http://www.armaholic.com/page.php?id=26209

-------------------------------------------------------------------*/

_plane = _this select 0;

/*------------------------------------------------------
 check if crew or pilot is trying to access script
 use the following expression for human players only:
 if(player != driver _plane) then {
 for human players and AI use:
 if(isNull driver _plane) then {
 ------------------------------------------------------*/

if(isNull driver _plane) then {
		_plane vehiclechat "Take your hands off the JATO System crewman! What are you doing?";
} else {


	
	call compile preprocessFileLineNumbers "\sab_C130\scripts\jatoparticlesEffect.sqf";
	
	// remove the action and initialise a few variables
	_plane removeAction jato;
	_speed = 3;   // play with this to increase the power of the acceleration
	_rockettype = "Sub_F_Signal_Red"; // Effect object
	_boostcycle = 0;
	
	_plane vehiclechat "JATO System ignition in";
	_plane vehiclechat "2";
	sleep 1;
	_plane vehiclechat "1";
	sleep 1;
	_plane vehiclechat "Ignition!";
	
	_plane animate["proxies_jato", 0]; 	
	_jatoparticleeffects = [_plane] execvm "\sab_C130\scripts\jatoparticles.sqf";
	
	while {_boostcycle < 25 }  do {	
		sleep 0.3; // interval at which the boost happens
		_boostcycle = _boostcycle+1; 
		_vel = velocity _plane;  // velocity of aircraft at current time 
		_dir = direction _plane; // vector of aircraft at current time
		// acceleration happens here, I got the idea from http://www.armaholic.com/forums.php?m=posts&q=22758
		_plane setVelocity [(_vel select 0)+(sin _dir*_speed),(_vel select 1)+ (cos _dir*_speed),(_vel select 2)];  
		
		if (_boostcycle == 1) then {
			// next step is attaching our effects
			rocket1a = _rockettype createVehicle position _plane;
			rocket2a = _rockettype createVehicle position _plane;
			rocket1a attachTo [_plane,[2.26,-1.1,-3.45]];
			rocket1a setVectorDirAndUp [[0.14,0.9,0.1],[0.86,0.1,0.9]];   
			rocket2a attachTo [_plane,[-2.26,-1.1,-3.45]];
			rocket2a setVectorDirAndUp [[-0.14,0.9,0.1],[0.86,0.1,0.9]];
		};
		if (_boostcycle == 10) then {
			
			rocket1b = _rockettype createVehicle position _plane;
			rocket2b = _rockettype createVehicle position _plane;
			rocket1b attachTo [_plane,[2.26,-1.1,-3.45]];
			rocket2b setVectorDirAndUp [[0.14,0.9,0.1],[0.86,0.1,0.9]];   
			rocket2b attachTo [_plane,[-2.26,-1.1,-3.45]];
			rocket2b setVectorDirAndUp [[-0.14,0.9,0.1],[0.86,0.1,0.9]];
		};
		if (_boostcycle == 20) then {
			
			rocket1c = _rockettype createVehicle position _plane;
			rocket2c = _rockettype createVehicle position _plane;
			rocket1c attachTo [_plane,[2.26,-1.1,-3.45]];
			rocket2c setVectorDirAndUp [[0.14,0.9,0.1],[0.86,0.1,0.9]];   
			rocket2c attachTo [_plane,[-2.26,-1.1,-3.45]];
			rocket2c setVectorDirAndUp [[-0.14,0.9,0.1],[0.86,0.1,0.9]];
		};
		
	};
	_plane animate["proxies_jato", 1];	
	sleep 1.0;
	_plane vehiclechat "JATOs expended";
	sleep 5.0;
};