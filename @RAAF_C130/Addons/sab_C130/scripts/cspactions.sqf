/*-------------------------------------------------------------------
Modified for my C130 addon - based on my very simple JATO script
http://www.armaholic.com/page.php?id=26209
(Jet Assisted Take Off - actually Rocket Assisted Take Off)
should work in Multiplayer and on dedicated servers, can be a bit messy with multiple players/AI using it at the same time

use a trigger to reload. This will allow you to easily reload the JATO System.

Make a trigger, set it to <Activation Bluefor, Present, Repeatedly> and put this in its <Condition> line:    
{typeof _x == "sab_C130_CSP"} count thislist >0   
Adjust the type of C130 you are using, "sab_C130_CSP" for Credible Sport for example

In its <On Act> field put this: 
_xhandle = [(thislist select 0)] execVM "\sab_C130\scripts\CSPActions.sqf"; hint "Shrikes ready";


-------------------------------------------------------------------*/

_plane = _this select 0;
_plane animate["proxies_retro", 1]; 
_plane animate["proxies_jato", 1]; 
_plane animate["proxies_skis", 1];
_plane animate["reverseT_1", 0]; 
_plane animate["reverseT_2", 0]; 

/* if using a trigger to load the system, it's probably better to remove the action before adding it to prevent loading multiple actions*/

_plane removeAction CSPboost;
_plane removeAction CSPbrake;

CSPboost = _plane addaction ["Rocketboost","\sab_C130\scripts\CSPRocketLaunch.sqf"];
CSPbrake = _plane addaction ["Retrorockets","\sab_C130\scripts\CSPRetroRockets.sqf"];