//script to simulate mid air refueling
//originally created by Franze - modified by John_Spartan - adapted and simplified for C-130 by Sabre with permission 

// private["_PLANE_TANKER","_PLANE_CLIENT","_sensor","_refuel_probe","_refuel_drogue","_refuel_probe_pos","_refuel_drogue_pos","_Fuel_client","_vel_tanker","_vel_tanker_x","_vel_tanker_y","_vel_tanker_z"];

_PLANE_TANKER = _this select 0;
_PLANE_TANKER_PILOT = driver _PLANE_TANKER;

_sensor = "EmptyDetector" createVehicle [0,0,0];
_sensor attachTo [_PLANE_TANKER,[0,0,0],"refuel_drogue1"];

sleep 0.1;

_PLANE_CLIENT = _this select 1;
_PLANE_CLIENT_PILOT = driver _PLANE_CLIENT;

_refuel_probe = _PLANE_CLIENT selectionposition "refuel_probe";
_PLANE_CLIENT_PILOT sideChat "FUELSTATION - Requesting Refueling.";

sleep 2;

if (_refuel_probe isEqualTo [0,0,0]) exitWith { _PLANE_TANKER_PILOT sideChat "Your aircraft does not have a refueling probe."; }; sleep 0.5;

if (_PLANE_CLIENT animationPhase "fuel_probe" < 0.5) exitWith { _PLANE_TANKER_PILOT sideChat "You have to extended your refueling probe first."; }; sleep 0.5;

if (isNil "active_drogue" && _PLANE_TANKER animationPhase "fuel_drogue" == 0 && _PLANE_CLIENT animationPhase "fuel_probe" > 0.5) then { _PLANE_TANKER animate ["fuel_drogue",1]; active_drogue = "refuel_drogue1"; active_probe="refuel_probe"}; sleep 0.5;

if (isNil "active_drogue" ) exitWith { _PLANE_TANKER_PILOT sideChat "Wait your turn please."; }; sleep 0.5;




_PLANE_TANKER_PILOT sideChat "Positive. Drogue will be extended, commence refueling!"; sleep 0.5;





while {(alive _PLANE_TANKER) && (alive _PLANE_CLIENT) && (_PLANE_CLIENT animationPhase "fuel_probe" > 0.5)} do 
{
	_PLANE_TANKER animate ["fuel_drogue",1]; 
	
	_refuel_probe_pos = _PLANE_CLIENT ModelToWorld _refuel_probe;
		
	_refuel_drogue = _PLANE_TANKER selectionposition "refuel_drogue1";
	
	_refuel_drogue_pos = _PLANE_TANKER ModelToWorld _refuel_drogue;
	
	_distance = _refuel_probe_pos distance _refuel_drogue_pos;
	
	if( _distance <= 10)  exitWith { 
		_PLANE_TANKER_PILOT sideChat "You are now being refuelled. To cancel retract the probe.";
	};
	sleep 0.5;
};




/*
switch (typeOf _PLANE_CLIENT) do 
{		
		case "JS_JC_FA18E": {_PLANE_CLIENT attachTo [_PLANE_TANKER,[-0.8,-6.8,-0.8],"refuel_drogue1"];};
		case "JS_JC_FA18F": {_PLANE_CLIENT attachTo [_PLANE_TANKER,[-0.8,-6.5,-1.0],"refuel_drogue1"];};
		default {_PLANE_CLIENT attachTo [_PLANE_TANKER, [(0 - ((_PLANE_CLIENT selectionPosition active_probe) select 0)), (0 - ((_PLANE_CLIENT selectionPosition active_probe) select 1)), (0 - ((_PLANE_CLIENT selectionPosition active_probe) select 2))], active_drogue];};
};
*/



_PLANE_CLIENT attachTo [_PLANE_TANKER, [(0 - ((_PLANE_CLIENT selectionPosition "refuel_probe") select 0)), (0 - ((_PLANE_CLIENT selectionPosition "refuel_probe") select 1)), (0 - ((_PLANE_CLIENT selectionPosition "refuel_probe") select 2))], "refuel_drogue1"];




while {(alive _PLANE_TANKER) && (alive _PLANE_CLIENT) && (_PLANE_CLIENT animationphase "fuel_probe" > 0.5)} do 
{
	_Fuel_client = fuel _PLANE_CLIENT;
	_Fuel_TANKER = fuel _PLANE_TANKER;
	
	_PLANE_CLIENT setfuel (_Fuel_client + 0.02);
	
	If (_Fuel_client > 0.95) exitWith {};
	If (_Fuel_TANKER < 0.2) exitWith 	{_PLANE_TANKER_PILOT sideChat "Not enough Fuel";};
		
	_vel_tanker = velocity _PLANE_TANKER;
	_vel_tanker_x = _vel_tanker select 0;
	_vel_tanker_y = _vel_tanker select 1;
	_vel_tanker_z = _vel_tanker select 2;
	
	If (_vel_tanker_x > 350) exitWith {};
	
	If (_vel_tanker_y > 350) exitWith {};
	
	If (_vel_tanker_z > 350) exitWith {};
	
	sleep 1;
};




_PLANE_TANKER_PILOT sideChat "Refueling complete.";

sleep 1;

detach _PLANE_CLIENT;
_PLANE_TANKER animate ["fuel_drogue",0];
active_probe="";
deletevehicle _sensor;
exit;