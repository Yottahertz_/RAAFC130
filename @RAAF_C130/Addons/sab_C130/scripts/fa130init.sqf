/*-------------------------------------------------------------------
Modified for my C130 addon - based on my very simple JATO script
http://www.armaholic.com/page.php?id=26209
(Jet Assisted Take Off - actually Rocket Assisted Take Off)
should work in Multiplayer and on dedicated servers, can be a bit messy with multiple players/AI using it at the same time

use a trigger to reload. This will allow you to easily reload the JATO System.

Make a trigger, set it to <Activation Bluefor, Present, Repeatedly> and put this in its <Condition> line:    
{typeof _x == "sab_C130_FA"} count thislist >0   
Adjust the type of C130 you are using, "sab_C130_FA" for Fat Albert for example
In its <On Act> field put this: 
_xhandle = [(thislist select 0)] execVM "\sab_C130\scripts\FA_Init.sqf"; hint "JATO ready";


-------------------------------------------------------------------*/



_plane = _this select 0;

_plane animate["proxies_jbottles", 0];

/* if using a trigger to load the system, it's probably better to remove the action before adding it to prevent loading multiple actions*/

_plane removeAction jato;

jato = _plane addaction ["JATO","\sab_C130\scripts\jatoaction.sqf"];

sleep 10;