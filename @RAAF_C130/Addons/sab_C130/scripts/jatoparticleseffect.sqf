// inspired by Flannels RATO Script - effects part

jatoparticlesEffect = 
{
	private	["_veh","_effectSmoke1","_effectSmoke2","_effectFire1","_effectFire2"];
	_veh = [_this,0] call BIS_fnc_param;

	_effectSmoke1 = "#particlesource" createVehicleLocal (getPos _veh);
	_effectSmoke2 = "#particlesource" createVehicleLocal (getPos _veh);
	_effectSmoke1 setParticleClass "Missile4";
	_effectSmoke2 setParticleClass "Missile4";
	
	_effectFire1 = "#particlesource" createVehicleLocal (getPos _veh);
	_effectFire2 = "#particlesource" createVehicleLocal (getPos _veh);
	_effectFire1 setParticleClass "MissileDARExplo";
	_effectFire2 setParticleClass "MissileDARExplo";
	
	_col = [[0.7,0.7,0.7,0],[0.7,0.7,0.7,0.5],[0.7,0.7,0.7,0.4],[0.8,0.8,0.8,0.3],[0.9,0.9,0.9,0.15], [1,1,1,0]];
	
	_effectSmoke1 setParticleParams[["\A3\data_f\ParticleEffects\Universal\Universal",16,12,8],"","Billboard",1,0.5,[2.5,-7,-5],[0,0,-3],4,	4,0.1,0.1,[0.8,5,12],[[0.1,0.1,0.1,1],[0.3,0.3,0.3,0.3],[0.6,0.6,0.6,0]],[10],0.1,3,"","",_veh,1,true,0.1,[[0,0,0,0]]];	
	_effectSmoke2 setParticleParams[["\A3\data_f\ParticleEffects\Universal\Universal",16,12,8],"","Billboard",1,0.5,[-2.5,-7,-5],[0,0,-3],4,	4,0.1,0.1,[0.8,5,12],[[0.1,0.1,0.1,1],[0.3,0.3,0.3,0.3],[0.6,0.6,0.6,0]],[10],0.1,3,"","",_veh,1,true,0.1,[[0,0,0,0]]];

	_effectFire1 setParticleParams [["\A3\data_f\ParticleEffects\Universal\Universal",16,3,1],"",	"Billboard",1,0.04,[2.5,-7,-5],[0,0,-3],	1,1.275,1,0,[0.8,2],[[0.4,0.4,0.4,-2],[0.4,0.4,0.4,-1],[0.4,0.4,0.4,0]],[1000],1,0.0,"","",_veh,0.5,true,0.1,
		[[1.0, 0.8, 0.6, 1]]];
	_effectFire2 setParticleParams [["\A3\data_f\ParticleEffects\Universal\Universal",16,3,1],"",	"Billboard",1,0.04,[-2.5,-7,-5],[0,0,-3],	1,1.275,1,0,[0.8,2],[[0.4,0.4,0.4,-2],[0.4,0.4,0.4,-1],[0.4,0.4,0.4,0]],[1000],1,0.0,"","",_veh,0.5,true,0.1,
		[[1.0, 0.8, 0.6, 1]]];

	_sparklesEffect1 = "#lightpoint" createVehicleLocal (getPos _veh);
	_sparklesEffect1 setLightBrightness 0.6;
	_sparklesEffect1 setLightAmbient[1.0, 0.5, 0.15];
	_sparklesEffect1 setLightColor[1.0, 0.5, 0.2];
	_sparklesEffect1 lightAttachObject [_veh, [-3,-3,-9]];

	_sparklesEffect2 = "#lightpoint" createVehicleLocal (getPos _veh);
	_sparklesEffect2 setLightBrightness 0.6;
	_sparklesEffect2 setLightAmbient[1.0, 0.5, 0.15];
	_sparklesEffect2 setLightColor[1.0, 0.5, 0.2];
	_sparklesEffect2 lightAttachObject [_veh, [3,-3,-9]];

	sleep 12;
	deleteVehicle _effectSmoke1;
	deleteVehicle _effectSmoke2;
	deleteVehicle _effectFire1;
	deleteVehicle _effectFire2;
	deleteVehicle _sparklesEffect1;
	deleteVehicle _sparklesEffect2;
};