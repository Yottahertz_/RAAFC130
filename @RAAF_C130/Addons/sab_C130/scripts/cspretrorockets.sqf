/*-------------------------------------------------------------------
very simple JATO script for Sabre[Dust]s C130
based on my very simple JATO script
http://www.armaholic.com/page.php?id=26209

-------------------------------------------------------------------*/

_plane = _this select 0;

/*------------------------------------------------------
 check if crew or pilot is trying to access script
 use the following expression for human players only:
 if(player != driver _plane) then {
 for human players and AI use:
 if(isNull driver _plane) then {
 ------------------------------------------------------*/

if(isNull driver _plane) then {
		_plane vehiclechat "Seriously, take your hands off the Pilots controls ><";
} else {


	
	call compile preprocessFileLineNumbers "\sab_C130\scripts\CSPparticlesEffect_retro.sqf";
	
	// remove the action and initialise a few variables
	
	_plane removeAction CSPbrake;
	
	
	_speed = -4;   // play with this to increase the power of the deceleration
	_rockettype = "Sub_F_Signal_Red";  // Effect object
	_boostcycle = 0;
	
	_plane vehiclechat "Activation Retro Rockets in";
	_plane animate["reverseT_1", 1]; 
	_plane animate["reverseT_2", 1]; 
	_plane vehiclechat "1";
	sleep 1;
	_plane vehiclechat "Ignition";
	_plane animate["proxies_retro", 0]; 
	
	
	_cspretroeffect = [_plane] execvm "\sab_C130\scripts\CSPparticles_retro.sqf";
	
	while {_boostcycle < 11 }  do {
		
		sleep 0.3; // interval at which the boost happens
		_boostcycle = _boostcycle+1; 
		_vel = velocity _plane;  // velocity of aircraft at current time 
		_dir = direction _plane; // vector of aircraft at current time
		// deceleration happens here, I got the idea from http://www.armaholic.com/forums.php?m=posts&q=22758
		_plane setVelocity [(_vel select 0)+(sin _dir*_speed),(_vel select 1)+ (cos _dir*_speed),(_vel select 2)];  
	
		if (_boostcycle == 1) then {
		
			// next step is attaching our effects
			rocket1a = _rockettype createVehicle position _plane;
			rocket2a = _rockettype createVehicle position _plane;
			rocket1a attachTo [_plane,[2.26,-1.1,-3.45]];
			rocket1a setVectorDirAndUp [[0.14,0.9,0.1],[0.86,0.1,0.9]];   
			rocket2a attachTo [_plane,[-2.26,-1.1,-3.45]];
			rocket2a setVectorDirAndUp [[-0.14,0.9,0.1],[0.86,0.1,0.9]];

			retroEffect1 = _rockettype createVehicle position _plane;
			retroEffect1 attachTo [_plane,[0,0,-3]];
		};
	};
	//Effects only
	rocket1b = _rockettype createVehicle position _plane;
	rocket2b = _rockettype createVehicle position _plane;
	rocket1b attachTo [_plane,[2.26,-1.1,-3.45]];
	rocket2b setVectorDirAndUp [[0.14,0.9,0.1],[0.86,0.1,0.9]];   
	rocket2b attachTo [_plane,[-2.26,-1.1,-3.45]];
	rocket2b setVectorDirAndUp [[-0.14,0.9,0.1],[0.86,0.1,0.9]];
			
	_plane animate["proxies_retro", 1]; 
	
	sleep 2.0;
	_plane animate["reverseT_1", 0]; 
	_plane animate["reverseT_2", 0]; 
	_plane vehiclechat "Retro Rockets expended";
	sleep 1.0;
	

};