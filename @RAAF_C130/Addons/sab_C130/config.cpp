#include "basicDefines_A3.hpp"
class CfgPatches
{
	class sab_C130
	{
		author[] = {"Yottahertz."};
		authorUrl = "yottahertz.net";
		version = 0.9;
		units[] = {
            "sab_C130_JE",
            "sab_C130_JC",
            "sab_C130_J",
            "sab_C130_H",
            "sab_C130_HC"
        };
		weapons[] = {"ANGEL_CMFlareLauncher"};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Air_F","A3_Characters_F_Gamma"};
	};
};
class CfgFunctions
{ };
class CfgMovesBasic
{
	class DefaultDie;
	class ManActions
	{
		Sab_C130_Pilot = "Sab_C130_Pilot";
		C130_Cargo = "C130_Cargo";
		C130_Load = "C130_Load";
	};
};
class CfgMovesMaleSdr: CfgMovesBasic
{
	class States
	{
		class Crew;
		class sab_C130_accident_Pilot: DefaultDie
		{
			actions = "DeadActions";
			file = "\sab_C130\DATA\Anim\C130pilotKIA.rtm";
			speed = 0.5;
			looped = "false";
			terminal = 1;
			soundEnabled = 0;
			connectTo[] = {"DeadState",0.1};
		};
		class Sab_C130_Pilot: Crew
		{
			file = "\sab_C130\DATA\Anim\C130pilot.rtm";
			interpolateTo[] = {"sab_C130_accident_Pilot",1};
		};
	};
};
class ViewPilot;
class CfgVehicles
{
	class Strategic;
	class Air;
	class Plane: Air
	{
		class NewTurret;
		class Sounds;
		class AnimationSources;
		class HitPoints
		{
			class HitHull;
		};
		class MarkerLights;
	};
	class sab_C130_J_Base: Plane
	{
		model = "\sab_C130\c130_j.p3d";
		displayName = "Sabres C-130";
		icon = "sab_C130\data\icon_c130j_ca";
		picture = "sab_C130\data\picture_c130j_ca";
		mapSize = 35;
		extCameraPosition[] = {0,5,-30};
		gearRetracting = 1;
		cabinOpening = 1;
		fuelCapacity = 2000;
		maxFordingDepth = 1.5;
		driverAction = "Sab_C130_Pilot";
		hascommander = 0;
		driverIsCommander = 1;
		ejectDeadDriver = 1;
		hideWeaponsDriver = 1;
		getInRadius = 12;
		driverCompartments = "Compartment1";
		getinaction = "GetInLow";
		getoutaction = "GetInLow";
		memoryPointsGetInDriver = "pos driver";
		memoryPointsGetInDriverDir = "pos driver dir";
		enableManualFire = 0;
		driverLeftHandAnimName = "stick_leftwheel";
		driverRightHandAnimName = "stick_leftwheel";
		hasGunner = 1;
		cargoCompartments[] = {"Compartment2"};
		transportSoldier = 24;
		hideWeaponsCargo = 0;
		memoryPointsGetInCargo = "pos cargo";
		memoryPointsGetInCargoDir = "pos cargo dir";
		crewVulnerable = "true";
		cargocaneject = 1;	
		ejectDeadCargo = true;
		transportMaxWeapons = 15;
		transportMaxMagazines = 45;
		transportMaxBackpacks = 10;
		cargoGetInAction[] = {"GetOutLow"};
		cargoGetOutAction[] = {"GetOutLow"};
		cargoAction[]={"passenger_generic01_foldhands"};

		class Turrets {			
			// Copilot
			class MainTurret : NewTurret {
				// copilot
				isCopilot = 1;
				commanding = -1;
				hasGunner = true;
				gunnerName = "Co-Pilot";
				gunnerCompartments = "Compartment1";
				gunnerUsesPilotView = true;
				primaryObserver = 1;   // new 2016
				primaryGunner = 0;  // 1 - set to 0 2016
				gunnerlefthandanimname = "stick_rightwheel";
				gunnerrighthandanimname = "stick_rightwheel";
				proxyIndex = 1;
				memoryPointsGetInGunner = "pos gunner";
				memoryPointsGetInGunnerDir = "pos gunner dir";
				gunnergetinaction = "GetInLow";
				gunnergetoutaction = "GetOutHigh";
				caneject = 1;
				weapons[] = {};
				magazines[] = {};
				gunnerForceOptics = false;
				gunnerAction = "Sab_C130_Pilot";
				
				startEngine = false;
				minElev = -60;
				maxElev = 10;
				initElev = 0;
				minTurn = -70;
				maxTurn = 70;
				initTurn = 0;
				ejectDeadGunner = 0;
				class ViewPilot : ViewPilot {
					initFov = 1;
					minFov = 0.3;
					maxFov = 1.2;
					initAngleX = 0;
					minAngleX = -75;
					maxAngleX = 85;
					initAngleY = 0;
					minAngleY = -170;
					maxAngleY = 170;
					
				}; 
			};
			// Flight Engineer
			class MainTurret2 : NewTurret {
				// copilot
				commanding = -2;
				gunnerName = "Flight Engineer";
				gunnerCompartments = "Compartment1";
				gunnerUsesPilotView = true;
				primaryGunner = 0;
				isCopilot = 0; // New!
				proxyIndex = 3;
				memoryPointsGetInGunner = "pos gunner";
				memoryPointsGetInGunnerDir = "pos gunner dir";
				gunnergetinaction = "GetInLow";
				gunnergetoutaction = "GetOutHigh";
				caneject = 1;
				weapons[] = {};
				magazines[] = {};
				gunnerForceOptics = false;
				gunnerAction = "passenger_generic01_foldhands";
			    hasGunner = true;
				startEngine = false;
				minElev = -60;
				maxElev = 10;
				initElev = 0;
				minTurn = -70;
				maxTurn = 70;
				initTurn = 0;
				ejectDeadGunner = 0;
				class ViewPilot : ViewPilot {
					initFov = 1;
					minFov = 0.3;
					maxFov = 1.2;
					initAngleX = 0;
					minAngleX = -75;
					maxAngleX = 85;
					initAngleY = 0;
					minAngleY = -170;
					maxAngleY = 170;
				
				}; 
			};
			// Load Master
			class GunnerTurret : NewTurret {
				// door seat 
				enableCopilot = false;
				commanding = -3;
				gunnerName = "Load Master";
				gunnerCompartments = "Compartment1";
				gunnerUsesPilotView = false;
				gunnerAction = "Stand"; 
				hasGunner = true;
				primaryGunner = 0;
				isCopilot = 0; // New!
				proxyIndex = 2;
				memoryPointsGetInGunner = "pos gunner";
				memoryPointsGetInGunnerDir = "pos gunner dir";
				gunnergetinaction = "GetInLow";
				gunnergetoutaction = "GetOutHigh";
				caneject = 1;
				weapons[] = {};
				magazines[] = {};
				gunnerForceOptics = false;
				showAsCargo = 1;
				ejectDeadGunner = 1;
				isPersonTurret = 1;   // enable FFV
				startEngine = false;
				minElev = -40;
				maxElev = 10;
				initElev = 0;
				minTurn = -20;
				maxTurn = 60;
				initTurn = 0;
				
				class ViewPilot : ViewPilot {
					initFov = 1;
					minFov = 0.3;
					maxFov = 1.2;
					initAngleX = 0;
					minAngleX = -75;
					maxAngleX = 85;
					initAngleY = 0;
					minAngleY = -170;
					maxAngleY = 170;
				
				}; 

			};
		};
		driveOnComponent[] = {"wheel_1_1","wheel_2_1","wheel_2_2","wheel_3_1","wheel_3_2"};
		flapsFrictionCoef = 3;
		wheelSteeringSensitivity = 2;
		aileronSensitivity = 0.8;
		elevatorSensitivity = 1;
		noseDownCoef = 1;
		gearUpTime = 4;
		gearDownTime = 4;
		landingSpeed = 180;
		acceleration = 300;
		maxSpeed = 671;
		altFullForce = 12310;
		altNoForce = 8615;
		landingAoa = "rad 10";
		thrustCoef[] = {1.2,1.6,1.5,1.2,1.1,1,0.9,0.8,0.7,0.6,0.4,0.3,0.2,0.1,0,0};
		envelope[] = {0.3,0.5,1,1.8,2.8,3.3,3.5,3.2,2.6,2,1.5,1,0.5,0};
		rudderInfluence = 0.25;
		angleOfIndicence = 0.05235987;
		draconicForceXCoef = 2.5;
		draconicForceYCoef = 1;
		draconicForceZCoef = 0.5;
		draconicTorqueXCoef = 8;
		draconicTorqueYCoef = 1.5;
		canFloat = 0;
		WaterLeakiness = 0.5;
		waterResistanceCoef = 0.004;
		irScanRangeMin = 500;		
		irScanRangeMax = 5000;		
		irScanToEyeFactor = 2;		
		accuracy = 0.15;		
		armor = 65;					
		damageResistance = 0.00278;	
		destrType = DestructWreck;	
		threat[] = {1, 1, 1};		
		ejectSpeed[] = {0, 0, 0};
		laserScanner = true;	
		irScanGround = true;		
		radarType = 4;
		LockDetectionSystem = 2 + 4 + 8;
		incomingMissileDetectionSystem = 16;
		gunAimDown = 0.029000;	
		headAimDown = 0.0000;		
		minFireTime = 30;		
        weapons[] = {"ANGEL_CMFlareLauncher"};
		magazines[] = {"300Rnd_CMFlare_Chaff_Magazine"};
		memoryPointCM[] = {"flare_launcher1","flare_launcher2","flare_launcher3","flare_launcher4"};
		memoryPointCMDir[] = {"flare_launcher1_dir","flare_launcher2_dir","flare_launcher3_dir","flare_launcher4_dir"};
		damageEffect = "DamageSmokePlane";
		audible = 7;
		hiddenSelections[] = {"camo1","camo2"};
		hiddenSelectionsTextures[] = {"sab_C130\data\c130j_body_co.paa","sab_C130\data\c130j_wings_co.paa"};
		soundGetIn[] = {"\sab_C130\data\sounds\close",1,1};
		soundGetOut[] = {"\sab_C130\data\sounds\open",1,1};
		soundIncommingMissile[] = {"\A3\Sounds_F\weapons\Rockets\locked_3",0.1,1.5};
		soundGearUp[] = {"A3\Sounds_F_EPC\CAS_02\gear_up",0.794328,1,150};
		soundGearDown[] = {"A3\Sounds_F_EPC\CAS_02\gear_down",0.794328,1,150};
		soundFlapsUp[] = {"A3\Sounds_F_EPC\CAS_02\Flaps_Up",0.630957,1,100};
		soundFlapsDown[] = {"A3\Sounds_F_EPC\CAS_02\Flaps_Down",0.630957,1,100};
		soundEngineOnInt[] = {"\sab_C130\data\sounds\int_start_1",0.707946,1};
		soundEngineOnExt[] = {"\sab_C130\data\sounds\ext_start_1",0.707946,1,400};
		soundEngineOffInt[] = {"\sab_C130\data\sounds\int_stop_1",0.707946,1};
		soundEngineOffExt[] = {"\sab_C130\data\sounds\ext_stop_1",0.707946,1,400};
		class EventHandlers
		{};
		class Sounds
		{
			class EngineLowOut
			{
				sound[] = {"\sab_C130\data\sounds\ext_engine_low",0.70794576,1,450};
				frequency = "1.0 min (rpm + 0.5)";
				volume = "camPos*(rpm factor[0.95, 0])*(rpm factor[0, 0.95])";
			};
			class EngineHighOut
			{
				sound[] = {"\sab_C130\data\sounds\ext_engine_hi",1,1,650};
				frequency = "(rpm factor[0.5, 1.0])";
				volume = "camPos*(rpm factor[0.2, 1.0])";
			};
			class ForsageOut
			{
				sound[] = {"\sab_C130\data\sounds\ext_forsage_1",1.1220185,1,900};
				frequency = "1";
				volume = "engineOn*camPos*(thrust factor[0.6, 1.0])";
				cone[] = {3.14,3.92,2,0.5};
			};
			class WindNoiseOut
			{
				sound[] = {"A3\Sounds_F\air\UAV_02\noise",0.31622776,1,150};
				frequency = "(0.3+(1.005*(speed factor[1, 50])))";
				volume = "camPos*(speed factor[1,  50])";
			};
			class EngineLowIn
			{
				sound[] = {"\sab_C130\data\sounds\int_engine_low",1,1};
				frequency = "1.0 min (rpm + 0.5)";
				volume = "(1-camPos)*(rpm factor[0.95, 0])*(rpm factor[0, 0.95])";
			};
			class EngineHighIn
			{
				sound[] = {"\sab_C130\data\sounds\int_engine_hi",1,1};
				frequency = "(rpm factor[0.5, 1.0])";
				volume = "(1-camPos)*(rpm factor[0.2, 1.0])";
			};
			class ForsageIn
			{
				sound[] = {"\sab_C130\data\sounds\int_forsage_1",0.630957,1};
				frequency = "1";
				volume = "engineOn*(1-camPos)*(thrust factor[0.6, 1.0])";
			};
			class WindNoiseIn
			{
				sound[] = {"A3\Sounds_F\air\UAV_02\noise",0.25118864,1};
				frequency = "(0.3+(1.005*(speed factor[1, 50])))";
				volume = "(1-camPos)*(speed factor[1, 50])";
			};
		};
        
		class Exhausts 
		{
			class Exhaust1 
			{
				position = "exhaust1";  	
				direction = "exhaust1_dir";	
				effect = "ExhaustsEffectPlane";	
			};
			class Exhaust2
			{
				position = "exhaust2";  	
				direction = "exhaust2_dir";	
				effect = "ExhaustsEffectPlane";
			};			
			class Exhaust3
			{
				position = "exhaust3";  	
				direction = "exhaust3_dir";	
				effect = "ExhaustsEffectPlane";
			};			
			class Exhaust4
			{
				position = "exhaust4";  	
				direction = "exhaust4_dir";	
				effect = "ExhaustsEffectPlane";
			};			
		};

		class MFD
		{
			class AirplaneHUD
			{
				topLeft = "HUD LH";
				topRight = "HUD PH";
				bottomLeft = "HUD LD";
				borderLeft = 0;
				borderRight = 0;
				borderTop = 0;
				borderBottom = 0;
				color[] = {0,1,0,0.1};
				enableParallax = 0;
				class Bones
				{
					class PlaneOrientation
					{
						type = "fixed";
						pos[] = {0.5,0.53};
					};
					class WeaponAim
					{
						type = "vector";
						source = "weapon";
						pos0[] = {0.5,0.53};
						pos10[] = {1.5,1.53};
					};
					class Velocity
					{
						type = "vector";
						source = "velocity";
						pos0[] = {0.5,0.53};
						pos10[] = {1.5,1.53};
					};
					class Level0
					{
						type = "horizon";
						pos0[] = {0.5,0.53};
						pos10[] = {1.5,1.53};
						angle = 0;
					};
					class TerrainBone
					{
						type = "linear";
						source = "altitudeAGL";
						sourceScale = 1;
						min = 0;
						max = 200;
						minPos[] = {0,0.666};
						maxPos[] = {0,0.4};
					};
				};
				class Draw
				{
					color[] = {0,1,0};
					alpha = 0.9;
					condition = "on";
					class Horizont
					{
						clipTL[] = {0.25,0.25};
						clipBR[] = {0.75,0.75};
						class Dimmed
						{
							class Level00
							{
								type = "line";
								width = 4;
								points[] = {{"Level0",{"-0.200 * 1.4","0 * 1.4"},1},{"Level0",{"-0.125 * 1.4","0 * 1.4"},1},{},{"Level0",{"-0.045 * 1.4","0 * 1.4"},1},{"Level0",{"-0.005 * 1.4","0 * 1.4"},1},{},{"Level0",{"0.005 * 1.4","0 * 1.4"},1},{"Level0",{"0.045 * 1.4","0 * 1.4"},1},{},{"Level0",{"0.125 * 1.4","0 * 1.4"},1},{"Level0",{"0.200 * 1.4","0 * 1.4"},1}};
							};
							class Level2M5: Level00
							{
								type = "line";
								points[] = {{"Level0",{"-0.200 * 1.4","-1.0 / 10 * 5"},1},{"Level0",{"-0.125 * 1.4","-1.0 / 10 * 5"},1},{},{"Level0",{"+0.125 * 1.4","-1.0 / 10 * 5"},1},{"Level0",{"+0.200 * 1.4","-1.0 / 10 * 5"},1}};
							};
							class VALM2_1_5
							{
								type = "text";
								source = "static";
								text = 5;
								align = "center";
								scale = 1;
								sourceScale = 1;
								pos[] = {"Level0",{"0.000 * 1.4","-0.020 * 1.4 - 1.0 / 10 * 5"},1};
								right[] = {"Level0",{"0.050 * 1.4","-0.020 * 1.4 - 1.0 / 10 * 5"},1};
								down[] = {"Level0",{"0.000 * 1.4","+0.020 * 1.4 - 1.0 / 10 * 5"},1};
							};
							class Level2P5: Level00
							{
								type = "line";
								points[] = {{"Level0",{"-0.200 * 1.4","+1.0 / 10 * 5"},1},{"Level0",{"-0.125 * 1.4","+1.0 / 10 * 5"},1},{},{"Level0",{"+0.125 * 1.4","+1.0 / 10 * 5"},1},{"Level0",{"+0.200 * 1.4","+1.0 / 10 * 5"},1}};
							};
							class VALP2_1_5
							{
								type = "text";
								source = "static";
								text = -5;
								align = "center";
								scale = 1;
								sourceScale = 1;
								pos[] = {"Level0",{"0.000 * 1.4","-0.020 * 1.4 + 1.0 / 10 * 5"},1};
								right[] = {"Level0",{"0.050 * 1.4","-0.020 * 1.4 + 1.0 / 10 * 5"},1};
								down[] = {"Level0",{"0.000 * 1.4","+0.020 * 1.4 + 1.0 / 10 * 5"},1};
							};
							class Level2M10: Level00
							{
								type = "line";
								points[] = {{"Level0",{"-0.200 * 1.4","-1.0 / 10 * 10"},1},{"Level0",{"-0.125 * 1.4","-1.0 / 10 * 10"},1},{},{"Level0",{"+0.125 * 1.4","-1.0 / 10 * 10"},1},{"Level0",{"+0.200 * 1.4","-1.0 / 10 * 10"},1}};
							};
							class VALM2_1_10
							{
								type = "text";
								source = "static";
								text = 10;
								align = "center";
								scale = 1;
								sourceScale = 1;
								pos[] = {"Level0",{"0.000 * 1.4","-0.020 * 1.4 - 1.0 / 10 * 10"},1};
								right[] = {"Level0",{"0.050 * 1.4","-0.020 * 1.4 - 1.0 / 10 * 10"},1};
								down[] = {"Level0",{"0.000 * 1.4","+0.020 * 1.4 - 1.0 / 10 * 10"},1};
							};
							class Level2P10: Level00
							{
								type = "line";
								points[] = {{"Level0",{"-0.200 * 1.4","+1.0 / 10 * 10"},1},{"Level0",{"-0.125 * 1.4","+1.0 / 10 * 10"},1},{},{"Level0",{"+0.125 * 1.4","+1.0 / 10 * 10"},1},{"Level0",{"+0.200 * 1.4","+1.0 / 10 * 10"},1}};
							};
							class VALP2_1_10
							{
								type = "text";
								source = "static";
								text = -10;
								align = "center";
								scale = 1;
								sourceScale = 1;
								pos[] = {"Level0",{"0.000 * 1.4","-0.020 * 1.4 + 1.0 / 10 * 10"},1};
								right[] = {"Level0",{"0.050 * 1.4","-0.020 * 1.4 + 1.0 / 10 * 10"},1};
								down[] = {"Level0",{"0.000 * 1.4","+0.020 * 1.4 + 1.0 / 10 * 10"},1};
							};
							class Level2M15: Level00
							{
								type = "line";
								points[] = {{"Level0",{"-0.200 * 1.4","-1.0 / 10 * 15"},1},{"Level0",{"-0.125 * 1.4","-1.0 / 10 * 15"},1},{},{"Level0",{"+0.125 * 1.4","-1.0 / 10 * 15"},1},{"Level0",{"+0.200 * 1.4","-1.0 / 10 * 15"},1}};
							};
							class VALM2_1_15
							{
								type = "text";
								source = "static";
								text = 15;
								align = "center";
								scale = 1;
								sourceScale = 1;
								pos[] = {"Level0",{"0.000 * 1.4","-0.020 * 1.4 - 1.0 / 10 * 15"},1};
								right[] = {"Level0",{"0.050 * 1.4","-0.020 * 1.4 - 1.0 / 10 * 15"},1};
								down[] = {"Level0",{"0.000 * 1.4","+0.020 * 1.4 - 1.0 / 10 * 15"},1};
							};
							class Level2P15: Level00
							{
								type = "line";
								points[] = {{"Level0",{"-0.200 * 1.4","+1.0 / 10 * 15"},1},{"Level0",{"-0.125 * 1.4","+1.0 / 10 * 15"},1},{},{"Level0",{"+0.125 * 1.4","+1.0 / 10 * 15"},1},{"Level0",{"+0.200 * 1.4","+1.0 / 10 * 15"},1}};
							};
							class VALP2_1_15
							{
								type = "text";
								source = "static";
								text = -15;
								align = "center";
								scale = 1;
								sourceScale = 1;
								pos[] = {"Level0",{"0.000 * 1.4","-0.020 * 1.4 + 1.0 / 10 * 15"},1};
								right[] = {"Level0",{"0.050 * 1.4","-0.020 * 1.4 + 1.0 / 10 * 15"},1};
								down[] = {"Level0",{"0.000 * 1.4","+0.020 * 1.4 + 1.0 / 10 * 15"},1};
							};
						};
					};
					class PlaneOrientationCrosshair
					{
						type = "line";
						width = 4;
						points[] = {{"PlaneOrientation",{-0.125,0},1},{"PlaneOrientation",{-0.05,0},1},{"PlaneOrientation",{-0.025,0.05},1},{"PlaneOrientation",{0,0},1},{"PlaneOrientation",{0.025,0.05},1},{"PlaneOrientation",{0.05,0},1},{"PlaneOrientation",{0.125,0},1}};
					};
					class PlaneMovementCrosshair
					{
						type = "line";
						width = 4;
						points[] = {{"Velocity",{0,-0.02},1},{"Velocity",{0.01,-0.01732},1},{"Velocity",{0.01732,-0.01},1},{"Velocity",{0.02,0},1},{"Velocity",{0.01732,0.01},1},{"Velocity",{0.01,0.01732},1},{"Velocity",{0,0.02},1},{"Velocity",{-0.01,0.01732},1},{"Velocity",{-0.01732,0.01},1},{"Velocity",{-0.02,0},1},{"Velocity",{-0.01732,-0.01},1},{"Velocity",{-0.01,-0.01732},1},{"Velocity",{0,-0.02},1},{},{"Velocity",{0.04,0},1},{"Velocity",{0.02,0},1},{},{"Velocity",{-0.04,0},1},{"Velocity",{-0.02,0},1},{},{"Velocity",{0,-0.04},1},{"Velocity",{0,-0.02},1}};
					};
					class WeaponName
					{
						type = "text";
						source = "weapon";
						sourceScale = 1;
						align = "right";
						scale = 1;
						pos[] = {{0.03,0.94},1};
						right[] = {{0.08,0.94},1};
						down[] = {{0.03,0.98},1};
					};
					class AmmoCount
					{
						type = "text";
						source = "ammo";
						sourceScale = 1;
						align = "right";
						scale = 1;
						pos[] = {{0.03,0.89},1};
						right[] = {{0.08,0.89},1};
						down[] = {{0.03,0.93},1};
					};
					class FlapsGroup
					{
						type = "group";
						condition = "flaps";
						class FlapsText
						{
							type = "text";
							source = "static";
							text = "FLAPS";
							align = "right";
							scale = 1;
							pos[] = {{0.03,"0.53 - 0.045"},1};
							right[] = {{0.08,"0.53 - 0.045"},1};
							down[] = {{0.03,"0.53 - 0.005"},1};
						};
					};
					class GearGroup
					{
						type = "group";
						condition = "ils";
						class GearText
						{
							type = "text";
							source = "static";
							text = "GEAR";
							align = "right";
							scale = 1;
							pos[] = {{0.03,"0.53 + 0.005"},1};
							right[] = {{0.08,"0.53 + 0.005"},1};
							down[] = {{0.03,"0.53 + 0.045"},1};
						};
					};
					class StallGroup
					{
						type = "group";
						condition = "stall";
						color[] = {1,0,0};
						blinkingPattern[] = {0.2,0.2};
						blinkingStartsOn = 1;
						class StallText
						{
							type = "text";
							source = "static";
							text = "STALL";
							align = "center";
							scale = 1;
							pos[] = {{0.5,"0.53 - 0.25"},1};
							right[] = {{0.55,"0.53 - 0.25"},1};
							down[] = {{0.5,"0.53 - 0.21"},1};
						};
					};
					class LightsGroup
					{
						type = "group";
						condition = "lights";
						class LightsText
						{
							type = "text";
							source = "static";
							text = "LIGHTS";
							align = "right";
							scale = 1;
							pos[] = {{0.03,"0.53 + 0.055"},1};
							right[] = {{0.08,"0.53 + 0.055"},1};
							down[] = {{0.03,"0.53 + 0.095"},1};
						};
					};
					class CollisionLightsGroup
					{
						type = "group";
						condition = "collisionlights";
						class CollisionLightsText
						{
							type = "text";
							source = "static";
							text = "A-COL";
							align = "right";
							scale = 1;
							pos[] = {{0.03,"0.53 + 0.105"},1};
							right[] = {{0.08,"0.53 + 0.105"},1};
							down[] = {{0.03,"0.53 + 0.145"},1};
						};
					};
					class PitchNumber
					{
						type = "text";
						source = "horizonDive";
						sourceScale = 57.2958;
						align = "right";
						scale = 1;
						pos[] = {{0.51,0.89},1};
						right[] = {{0.56,0.89},1};
						down[] = {{0.51,0.93},1};
					};
					class PitchText
					{
						type = "text";
						source = "static";
						text = "PITCH";
						align = "left";
						scale = 1;
						pos[] = {{0.49,0.89},1};
						right[] = {{0.54,0.89},1};
						down[] = {{0.49,0.93},1};
					};
					class RollNumber
					{
						type = "text";
						source = "horizonBank";
						sourceScale = 57.2958;
						align = "right";
						scale = 1;
						pos[] = {{0.51,0.94},1};
						right[] = {{0.56,0.94},1};
						down[] = {{0.51,0.98},1};
					};
					class RollText
					{
						type = "text";
						source = "static";
						text = "ROLL";
						align = "left";
						scale = 1;
						pos[] = {{0.49,0.94},1};
						right[] = {{0.54,0.94},1};
						down[] = {{0.49,0.98},1};
					};
					class SpeedNumber
					{
						type = "text";
						source = "speed";
						sourceScale = 3.6;
						align = "right";
						scale = 1;
						pos[] = {{0.13,0.14},1};
						right[] = {{0.18,0.14},1};
						down[] = {{0.13,0.18},1};
					};
					class SpeedText
					{
						type = "text";
						source = "static";
						text = "SPD";
						align = "left";
						scale = 1;
						pos[] = {{0.11,0.14},1};
						right[] = {{0.16,0.14},1};
						down[] = {{0.11,0.18},1};
					};
					class TerrainGroup
					{
						type = "group";
						clipTL[] = {0,0};
						clipBR[] = {1,0.6};
						class TerrainArrow
						{
							type = "line";
							width = 4;
							points[] = {{{"0.86 - 0.018",0.4},1},{{0.86,0.4},1},{},{"TerrainBone",{"0.86 - 0.018","0.0 - 0.016"},1},{"TerrainBone",{0.86,0},1},{"TerrainBone",{"0.86 - 0.018","0.0 + 0.016"},1}};
						};
					};
					class TerrainText
					{
						type = "text";
						source = "static";
						text = "ATL";
						align = "left";
						scale = 1;
						pos[] = {{0.85,0.1},1};
						right[] = {{0.9,0.1},1};
						down[] = {{0.85,0.14},1};
					};
					class TerrainNumber
					{
						type = "text";
						source = "altitudeAGL";
						sourceScale = 1;
						align = "left";
						scale = 1;
						pos[] = {{0.85,0.14},1};
						right[] = {{0.9,0.14},1};
						down[] = {{0.85,0.18},1};
					};
					class AltitudeNumber
					{
						type = "text";
						source = "altitudeASL";
						sourceScale = 1;
						align = "right";
						scale = 1;
						pos[] = {{0.89,0.14},1};
						right[] = {{0.94,0.14},1};
						down[] = {{0.89,0.18},1};
					};
					class AltitudeText
					{
						type = "text";
						source = "static";
						text = "ASL";
						align = "right";
						scale = 1;
						pos[] = {{0.89,0.1},1};
						right[] = {{0.94,0.1},1};
						down[] = {{0.89,0.14},1};
					};
					class AltitudeArrow
					{
						type = "line";
						width = 4;
						points[] = {{{"0.88 + 0.018","0.40 - 0.016"},1},{{0.88,0.4},1},{{"0.88 + 0.018","0.40 + 0.016"},1}};
					};
					class AltitudeLine
					{
						type = "line";
						width = 4;
						points[] = {{{0.87,0.6},1},{{0.87,0.2},1}};
					};
					class AltitudeScale
					{
						type = "scale";
						horizontal = 0;
						source = "altitudeASL";
						sourceScale = 1;
						width = 4;
						top = 0.6;
						center = 0.4;
						bottom = 0.2;
						lineXleft = 0.88;
						lineYright = 0.89;
						lineXleftMajor = 0.88;
						lineYrightMajor = 0.9;
						majorLineEach = 5;
						numberEach = 5;
						step = 20;
						stepSize = "(0.60 - 0.20) / 15";
						align = "right";
						scale = 1;
						pos[] = {0.91,0.58};
						right[] = {0.96,0.58};
						down[] = {0.91,0.62};
					};
					class ClimbNumber
					{
						type = "text";
						source = "vspeed";
						sourceScale = 1;
						align = "right";
						scale = 1;
						pos[] = {{0.89,0.94},1};
						right[] = {{0.94,0.94},1};
						down[] = {{0.89,0.98},1};
					};
					class ClimbText
					{
						type = "text";
						source = "static";
						text = "CLIMB";
						align = "left";
						scale = 1;
						pos[] = {{0.87,0.94},1};
						right[] = {{0.92,0.94},1};
						down[] = {{0.87,0.98},1};
					};
					class ClimbArrow
					{
						type = "line";
						width = 4;
						points[] = {{{"0.88 + 0.018","0.80 - 0.016"},1},{{0.88,0.8},1},{{"0.88 + 0.018","0.80 + 0.016"},1}};
					};
					class ClimbLine
					{
						type = "line";
						width = 4;
						points[] = {{{0.87,0.925},1},{{0.87,0.675},1}};
					};
					class ClimbScale
					{
						type = "scale";
						horizontal = 0;
						source = "vspeed";
						sourceScale = 1;
						width = 4;
						top = 0.925;
						center = 0.8;
						bottom = 0.675;
						lineXleft = 0.88;
						lineYright = 0.89;
						lineXleftMajor = 0.88;
						lineYrightMajor = 0.9;
						majorLineEach = 2;
						numberEach = 4;
						step = 5;
						stepSize = "(0.925 - 0.675) / 15";
						align = "right";
						scale = 1;
						pos[] = {0.91,0.905};
						right[] = {0.96,0.905};
						down[] = {0.91,0.945};
					};
					class HeadingArrow
					{
						type = "line";
						width = 3;
						points[] = {{{"0.5 - 0.02","0.11 - 0.02"},1},{{0.5,0.11},1},{{"0.5 + 0.02","0.11 - 0.02"},1}};
					};
					class HeadingLine
					{
						type = "line";
						width = 4;
						points[] = {{{0.3,0.12},1},{{0.7,0.12},1}};
					};
					class HeadingScale
					{
						type = "scale";
						horizontal = 1;
						source = "heading";
						sourceScale = 1;
						width = 4;
						top = 0.3;
						center = 0.5;
						bottom = 0.7;
						lineXleft = "0.06 + 0.05";
						lineYright = "0.05 + 0.05";
						lineXleftMajor = "0.06 + 0.05";
						lineYrightMajor = "0.04 + 0.05";
						majorLineEach = 5;
						numberEach = 5;
						step = "18 / 9";
						stepSize = "(0.70 - 0.3) / 15";
						align = "center";
						scale = 1;
						pos[] = {0.3,"0.0 + 0.05"};
						right[] = {0.35,"0.0 + 0.05"};
						down[] = {0.3,"0.04 + 0.05"};
					};
					class AAMissileCrosshairGroup
					{
						type = "group";
						condition = "AAmissile";
						class AAMissileCrosshair
						{
							type = "line";
							width = 4;
							points[] = {{"WeaponAim",{0,-0.25},1},{"WeaponAim",{0.0434,-0.2462},1},{"WeaponAim",{0.0855,-0.234925},1},{"WeaponAim",{0.125,-0.2165},1},{"WeaponAim",{0.1607,-0.1915},1},{"WeaponAim",{0.1915,-0.1607},1},{"WeaponAim",{0.2165,-0.125},1},{"WeaponAim",{0.234925,-0.0855},1},{"WeaponAim",{0.2462,-0.0434},1},{"WeaponAim",{0.25,0},1},{"WeaponAim",{0.2462,0.0434},1},{"WeaponAim",{0.234925,0.0855},1},{"WeaponAim",{0.2165,0.125},1},{"WeaponAim",{0.1915,0.1607},1},{"WeaponAim",{0.1607,0.1915},1},{"WeaponAim",{0.125,0.2165},1},{"WeaponAim",{0.0855,0.234925},1},{"WeaponAim",{0.0434,0.2462},1},{"WeaponAim",{0,0.25},1},{"WeaponAim",{-0.0434,0.2462},1},{"WeaponAim",{-0.0855,0.234925},1},{"WeaponAim",{-0.125,0.2165},1},{"WeaponAim",{-0.1607,0.1915},1},{"WeaponAim",{-0.1915,0.1607},1},{"WeaponAim",{-0.2165,0.125},1},{"WeaponAim",{-0.234925,0.0855},1},{"WeaponAim",{-0.2462,0.0434},1},{"WeaponAim",{-0.25,0},1},{"WeaponAim",{-0.2462,-0.0434},1},{"WeaponAim",{-0.234925,-0.0855},1},{"WeaponAim",{-0.2165,-0.125},1},{"WeaponAim",{-0.1915,-0.1607},1},{"WeaponAim",{-0.1607,-0.1915},1},{"WeaponAim",{-0.125,-0.2165},1},{"WeaponAim",{-0.0855,-0.234925},1},{"WeaponAim",{-0.0434,-0.2462},1},{"WeaponAim",{0,-0.25},1}};
						};
					};
					class ATMissileCrosshairGroup
					{
						condition = "ATmissile";
						type = "group";
						class ATMissileCrosshair
						{
							type = "line";
							width = 4;
							points[] = {{"WeaponAim",{-0.15,-0.15},1},{"WeaponAim",{-0.15,"-0.15 + 0.02"},1},{},{"WeaponAim",{-0.15,0.15},1},{"WeaponAim",{-0.15,"0.15 - 0.02"},1},{},{"WeaponAim",{0.15,-0.15},1},{"WeaponAim",{0.15,"-0.15 + 0.02"},1},{},{"WeaponAim",{0.15,0.15},1},{"WeaponAim",{0.15,"0.15 - 0.02"},1},{},{"WeaponAim",{-0.15,-0.15},1},{"WeaponAim",{"-0.15 + 0.02",-0.15},1},{},{"WeaponAim",{-0.15,0.15},1},{"WeaponAim",{"-0.15 + 0.02",0.15},1},{},{"WeaponAim",{0.15,-0.15},1},{"WeaponAim",{"0.15 - 0.02",-0.15},1},{},{"WeaponAim",{0.15,0.15},1},{"WeaponAim",{"0.15 - 0.02",0.15},1}};
						};
					};
					class BombCrosshairGroup
					{
						type = "group";
						condition = "bomb";
						class BombCrosshair
						{
							type = "line";
							width = 4;
							points[] = {{"WeaponAim",{0,-0.1},1},{"WeaponAim",{0.01736,-0.09848},1},{"WeaponAim",{0.0342,-0.09397},1},{"WeaponAim",{0.05,-0.0866},1},{"WeaponAim",{0.06428,-0.0766},1},{"WeaponAim",{0.0766,-0.06428},1},{"WeaponAim",{0.0866,-0.05},1},{"WeaponAim",{0.09397,-0.0342},1},{"WeaponAim",{0.09848,-0.01736},1},{"WeaponAim",{0.1,0},1},{"WeaponAim",{0.09848,0.01736},1},{"WeaponAim",{0.09397,0.0342},1},{"WeaponAim",{0.0866,0.05},1},{"WeaponAim",{0.0766,0.06428},1},{"WeaponAim",{0.06428,0.0766},1},{"WeaponAim",{0.05,0.0866},1},{"WeaponAim",{0.0342,0.09397},1},{"WeaponAim",{0.01736,0.09848},1},{"WeaponAim",{0,0.1},1},{"WeaponAim",{-0.01736,0.09848},1},{"WeaponAim",{-0.0342,0.09397},1},{"WeaponAim",{-0.05,0.0866},1},{"WeaponAim",{-0.06428,0.0766},1},{"WeaponAim",{-0.0766,0.06428},1},{"WeaponAim",{-0.0866,0.05},1},{"WeaponAim",{-0.09397,0.0342},1},{"WeaponAim",{-0.09848,0.01736},1},{"WeaponAim",{-0.1,0},1},{"WeaponAim",{-0.09848,-0.01736},1},{"WeaponAim",{-0.09397,-0.0342},1},{"WeaponAim",{-0.0866,-0.05},1},{"WeaponAim",{-0.0766,-0.06428},1},{"WeaponAim",{-0.06428,-0.0766},1},{"WeaponAim",{-0.05,-0.0866},1},{"WeaponAim",{-0.0342,-0.09397},1},{"WeaponAim",{-0.01736,-0.09848},1},{"WeaponAim",{0,-0.1},1},{},{"Velocity",0.001,"WeaponAim",{0,0},1},{"Velocity",{0,0},1}};
						};
					};
				};
			};
		};
		class ViewPilot: ViewPilot
		{
			initFov = 1;
			minFov = 0.3;
			maxFov = 1.2;
			initAngleX = 0;
			minAngleX = -75;
			maxAngleX = 85;
			initAngleY = 0;
			minAngleY = -170;
			maxAngleY = 170;
			initMoveX = 0;
			initMoveY = 0;
			initMoveZ = 0;
			minMoveX = -0.2;
			maxMoveX = 0.2;
			minMoveY = -0.025;
			maxMoveY = 0.1;
			minMoveZ = -0.2;
			maxMoveZ = 0.2;
		};
		class HitPoints: HitPoints
		{
			class HitTurret
			{
				armor = 0.8;
				material = 51;
				name = "turret";
				visual = "turret";
				passThrough = 1;
			};
			class HitGun
			{
				armor = 0.6;
				material = 52;
				name = "gun";
				visual = "gun";
				passThrough = 1;
			};
		};
		class AnimationSources
		{
			class door_2_2
			{
				source = "user";
				animPeriod = 2.5;
			};
			class door_2_1: door_2_2{};
			class door_1: door_2_2{};
			class ramp_top: door_2_2{};
			class ramp_bottom: door_2_2{};
			class proxies_jato
			{
				source = "user";
				animPeriod = 0.001;
				initPhase = 0;
			};
			class proxies_retro
			{
				source = "user";
				animPeriod = 0.001;
				initPhase = 0;
			};
			class proxies_skis
			{
				source = "user";
				animPeriod = 0.001;
				initPhase = 0;
			};
			class proxies_rpods
			{
				source = "user";
				animPeriod = 0.001;
				initPhase = 1;
			};
			class proxies_jbottles
			{
				source = "user";
				animPeriod = 0.001;
				initPhase = 0;
			};
			class proxies_droptanks
			{
				source = "user";
				animPeriod = 0.001;
				initPhase = 0;
			};
			class proxies_seats
			{
				source = "user";
				animPeriod = 0.001;
				initPhase = 0;
			};
			class proxies_flag
			{
				source = "user";
				animPeriod = 2;
				initPhase = 1;
			};
			class reverseT_1
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class reverseT_2
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class fuel_probe
			{
				source = "user";
				animPeriod = 2.5;
				initPhase = 1;
			};
			class fuel_drogue
			{
				source = "user";
				animPeriod = 10;
				initPhase = 0;
			};
			class fuel_drogue_h
			{
				source = "user";
				animPeriod = 0.1;
				initPhase = 1;
			};
		};
		class UserActions
		{
			class OpenRamp
			{
				displayName = "Open Cargo Ramp";
				position = "pos cargo";
				onlyforplayer = 1;
				radius = 15;
				showwindow = 0;
				condition = "(this animationPhase ""ramp_bottom"" == 0) AND Alive(this)";
				statement = "this animate [""ramp_bottom"",1];this animate [""ramp_top"",1];";
			};
			class CloseRamp: OpenRamp
			{
				displayName = "Close Cargo Ramp";
				condition = "(this animationPhase ""ramp_bottom"" == 1) AND Alive(this)";
				statement = "this animate [""ramp_bottom"",0];this animate [""ramp_top"",0];";
			};
			class OpenLdoor
			{
				displayName = "Open Left Jump Door";
				position = "pos cargo";
				onlyforplayer = 1;
				radius = 15;
				showwindow = 0;
				condition = "(this animationPhase ""door_2_1"" == 0) AND Alive(this)";
				statement = "this animate [""door_2_1"",1]";
			};
			class OpenRdoor: OpenLdoor
			{
				displayName = "Open Right Jump Door";
				condition = "(this animationPhase ""door_2_2"" == 0) AND Alive(this)";
				statement = "this animate [""door_2_2"",1]";
			};
			class Closeldoor: OpenLdoor
			{
				displayName = "Close Left Jump Door";
				condition = "(this animationPhase ""door_2_1"" == 1) AND Alive(this)";
				statement = "this animate [""door_2_1"",0]";
			};
			class Closerdoor: OpenLdoor
			{
				displayName = "Close Right Jump Door";
				condition = "(this animationPhase ""door_2_2"" == 1) AND Alive(this)";
				statement = "this animate [""door_2_2"",0]";
			};
			class openfdoor: OpenLdoor
			{
				displayName = "Open Crew Door";
				condition = "(this animationPhase ""door_1"" == 0) AND Alive(this)";
				statement = "this animate [""door_1"",1]";
			};
			class closefdoor: OpenLdoor
			{
				displayName = "Close Crew Door";
				condition = "(this animationPhase ""door_1"" == 1) AND Alive(this)";
				statement = "this animate [""door_1"",0]";
			};
		};
		class Reflectors
		{
			class sab_Left
			{
				color[] = {1900,1800,1700};
				ambient[] = {5,5,5};
				position = "L svetlo";
				direction = "L svetlo dir";
				hitpoint = "L svetlo";
				selection = "L svetlo";
				size = 1;
				innerAngle = 70;
				outerAngle = 120;
				coneFadeCoef = 10;
				intensity = 1;
				useFlare = 1;
				dayLight = 0;
				flareSize = 1;
				class Attenuation
				{
					start = 1;
					constant = 0;
					linear = 0;
					quadratic = 0.25;
					hardLimitStart = 30;
					hardLimitEnd = 60;
				};
			};
			class sab_Right: sab_Left
			{
				position = "P svetlo";
				direction = "P svetlo dir";
				hitpoint = "P svetlo";
				selection = "P svetlo";
			};
			class sab_Left2: sab_Left
			{
				position = "L2 svetlo";
				direction = "L svetlo dir";
				hitpoint = "L2 svetlo";
				selection = "L2 svetlo";
			};
			class sab_Right2: sab_Left
			{
				position = "P2 svetlo";
				direction = "P svetlo dir";
				hitpoint = "P2 svetlo";
				selection = "P2 svetlo";
			};
		};
		class MarkerLights
		{
			class sab_RedStill
			{
				name = "PositionLight_red";
				color[] = {1,0.1,0.1,1};
				ambient[] = {0.1,0.01,0.01,1};
				brightness = 0.1;
				blinking = 0;
			};
			class sab_GreenStill
			{
				name = "PositionLight_green";
				color[] = {0.1,1,0.1,1};
				ambient[] = {0.01,0.1,0.01,1};
				brightness = 0.1;
				blinking = 0;
			};
			class sab_WhiteCollision
			{
				name = "CollisionLight";
				color[] = {1,1,1,1};
				ambient[] = {0.1,0.1,0.1,1};
				brightness = 0.1;
				blinking = 1;
			};
		};
		class Damage
		{
			tex[] = {};
			mat[] = {"sab_C130\DATA\c130j_sklo.rvmat","sab_C130\DATA\c130j_sklo_damage.rvmat","sab_C130\DATA\c130j_sklo_damage.rvmat","sab_C130\DATA\c130j_sklo_in.rvmat","sab_C130\DATA\c130j_sklo_in_damage.rvmat","sab_C130\DATA\c130j_sklo_in_damage.rvmat","sab_C130\DATA\c130j_body.rvmat","sab_C130\DATA\c130j_body_damage.rvmat","sab_C130\DATA\c130j_body_destruct.rvmat","sab_C130\DATA\c130j_interior.rvmat","sab_C130\DATA\c130j_interior_damage.rvmat","sab_C130\DATA\c130j_interior_destruct.rvmat","sab_C130\DATA\c130j_wings.rvmat","sab_C130\DATA\c130j_wings_damage.rvmat","sab_C130\DATA\c130j_wings_destruct.rvmat","sab_C130\data\default.rvmat","sab_C130\data\default.rvmat","sab_C130\data\default_destruct.rvmat"};
		};
		class TransportBackpacks
		{
			class _xx_B_Parachute
			{
				backpack = "B_Parachute";
				count = 3;
			};
		};
	};
	class sab_C130_J: sab_C130_J_Base
	{
		side = 1;
		faction = "BLU_F";
		crew = "B_Pilot_F";
		vehicleClass = "air";
		author = "Yottahertz.";
		scope = 2;
		displayName = "RAAF Lockheed C-130J Super Hercules ";
		model = "\sab_C130\C130_J.p3d";
		class AnimationSources: AnimationSources
		{
			class proxies_droptanks: proxies_droptanks
			{
				initPhase = 1;
			};
			class proxies_skis: proxies_skis
			{
				initPhase = 1;
			};
			class proxies_jato: proxies_jato
			{
				initPhase = 1;
			};
			class proxies_jbottles: proxies_jbottles
			{
				initPhase = 1;
			};
			class proxies_seats: proxies_seats
			{
				initPhase = 0;
			};
		};
	};
	class sab_C130_JC: sab_C130_J_Base
	{
		side = 1;
		faction = "BLU_F";
		crew = "B_Pilot_F";
		vehicleClass = "air";
		author = "Yottahertz.";
		scope = 2;
		displayName = "RAAF Lockheed C-130J Cargo";
		model = "\sab_C130\C130_J.p3d";
		fuelCapacity = 2400;
		transportSoldier = 0;
		class AnimationSources: AnimationSources
		{
			class proxies_droptanks: proxies_droptanks
			{
				initPhase = 1;
			};
			class proxies_skis: proxies_skis
			{
				initPhase = 1;
			};
			class proxies_jato: proxies_jato
			{
				initPhase = 1;
			};
			class proxies_jbottles: proxies_jbottles
			{
				initPhase = 1;
			};
			class proxies_seats: proxies_seats
			{
				initPhase = 1;
			};
        };
            
        class VehicleTransport
		{
			class Carrier
			{
			cargoBayDimensions[]        = {"VTV_limit_1", "VTV_limit_2"};   // Memory points in model defining cargo space
			disableHeightLimit          = 1;                             // If set to 1 disable height limit of transported vehicles
			maxLoadMass                 = 33000;                           // Maximum cargo weight (in Kg) which the vehicle can transport
			cargoAlignment[]            = {"center", "front"};                // Array of 2 elements defining alignment of vehicles in cargo space. Possible values are left, right, center, front, back. Order is important.
			cargoSpacing[]              = {0, 0.15, 0};                     // Offset from X,Y,Z axes (in metres)
			exits[]                     = {"VTV_exit_1"};     // Memory points in model defining loading ramps, could have multiple
			unloadingInterval           = 2;                                // Time between unloading vehicles (in seconds)
			loadingDistance             = 15;                               // Maximal distance for loading in exit point (in meters).
			loadingAngle                = 60;                               // Maximal sector where cargo vehicle must be to for loading (in degrees).
			parachuteClassDefault       = B_Parachute_02_F;                 // Type of parachute used when dropped in air. Can be overridden by parachuteClass in Cargo.
			parachuteHeightLimitDefault = 50;                               // Minimal height above terrain when parachute is used. Can be overriden by parachuteHeightLimit in Cargo.
			}; 
        };
	};
	class sab_C130_H: sab_C130_J
	{
		side = 1;
		faction = "BLU_F";
		crew = "B_Pilot_F";
		vehicleClass = "air";
		author = "Yottahertz.";
		scope = 2;
		displayName = "RAAF Lockheed C-130H Hercules";
		model = "\sab_C130\C130_H.p3d";
		thrustCoef[] = {1,1.3,1.4,1.1,1,1,0.9,0.8,0.7,0.6,0.4,0.3,0.2,0.1,0,0};
		maxSpeed = 592;
		altFullForce = 8000;
		altNoForce = 10000;
		fuelCapacity = 2200;
		class AnimationSources: AnimationSources
        {
			class proxies_droptanks: proxies_droptanks
			{
				initPhase = 1;
			};
			class proxies_skis: proxies_skis
			{
				initPhase = 1;
			};
			class proxies_jato: proxies_jato
			{
				initPhase = 1;
			};
			class proxies_jbottles: proxies_jbottles
			{
				initPhase = 1;
			};
			class proxies_seats: proxies_seats
			{
				initPhase = 0;
			};
        };
	};
	class sab_C130_HC: sab_C130_JC
	{
		side = 1;
		faction = "BLU_F";
		crew = "B_Pilot_F";
		vehicleClass = "air";
		author = "Yottahertz.";
		scope = 2;
		displayName = "RAAF Lockheed C-130H Cargo";
		model = "\sab_C130\C130_H.p3d";
		thrustCoef[] = {1,1.3,1.4,1.1,1,1,0.9,0.8,0.7,0.6,0.4,0.3,0.2,0.1,0,0};
		maxSpeed = 592;
		altFullForce = 8000;
		altNoForce = 10000;
		fuelCapacity = 2200;
		class EventHandlers
		{};
		class AnimationSources: AnimationSources
        {
			class proxies_droptanks: proxies_droptanks
			{
				initPhase = 1;
			};
			class proxies_skis: proxies_skis
			{
				initPhase = 1;
			};
			class proxies_jato: proxies_jato
			{
				initPhase = 1;
			};
			class proxies_jbottles: proxies_jbottles
			{
				initPhase = 1;
			};
			class proxies_seats: proxies_seats
			{
				initPhase = 1;
			};
        };        
        class VehicleTransport
		{
			class Carrier
			{
			cargoBayDimensions[]        = {"VTV_limit_1", "VTV_limit_2"};   // Memory points in model defining cargo space
			disableHeightLimit          = 1;                             // If set to 1 disable height limit of transported vehicles
			maxLoadMass                 = 20,400;                           // Maximum cargo weight (in Kg) which the vehicle can transport
			cargoAlignment[]            = {"center", "front"};                // Array of 2 elements defining alignment of vehicles in cargo space. Possible values are left, right, center, front, back. Order is important.
			cargoSpacing[]              = {0, 0.15, 0};                     // Offset from X,Y,Z axes (in metres)
			exits[]                     = {"VTV_exit_1"};     // Memory points in model defining loading ramps, could have multiple
			unloadingInterval           = 2;                                // Time between unloading vehicles (in seconds)
			loadingDistance             = 15;                               // Maximal distance for loading in exit point (in meters).
			loadingAngle                = 60;                               // Maximal sector where cargo vehicle must be to for loading (in degrees).
			parachuteClassDefault       = B_Parachute_02_F;                 // Type of parachute used when dropped in air. Can be overridden by parachuteClass in Cargo.
			parachuteHeightLimitDefault = 50;                               // Minimal height above terrain when parachute is used. Can be overriden by parachuteHeightLimit in Cargo.
			}; 
        };
	};
};
class Mode_SemiAuto;
class Mode_Burst;
class Mode_FullAuto;
class Burst;
class cfgWeapons
{
	class CMFlareLauncher;
	class ANGEL_CMFlareLauncher: CMFlareLauncher
	{
		displayName = "Countermeasures";
		magazines[] = {"60Rnd_CMFlareMagazine","120Rnd_CMFlareMagazine","240Rnd_CMFlareMagazine","60Rnd_CMFlare_Chaff_Magazine","120Rnd_CMFlare_Chaff_Magazine","240Rnd_CMFlare_Chaff_Magazine","192Rnd_CMFlare_Chaff_Magazine","168Rnd_CMFlare_Chaff_Magazine","300Rnd_CMFlare_Chaff_Magazine","320Rnd_CMFlare_Chaff_Magazine"};
		magazineReloadTime = 10;
		modes[] = {"Single","Burst1","Burst2","Burst3","Burst4","Burst5","Burst6","AIBurst"};
		cursor = "EmptyCursor";
		cursorAim = "EmptyCursor";
		class Single: Mode_SemiAuto
		{
			reloadTime = 0.125;
			autoFire = "false";
			displayName = "A-4F/-";
			burst = 1;
			multiplier = 4;
			sounds[] = {"StandardSound"};
			class StandardSound
			{
				begin1[] = {"A3\Sounds_F\weapons\HMG\HMG_grenade",1,1,300};
				soundBegin[] = {"begin1",1};
				weaponSoundEffect = "DefaultRifle";
			};
		};
		class Burst1: Mode_Burst
		{
			reloadTime = 0.125;
			autoFire = "true";
			displayName = "B-40F/1.25S";
			burst = 10;
			multiplier = 4;
			sounds[] = {"StandardSound"};
			class StandardSound
			{
				begin1[] = {"A3\Sounds_F\weapons\HMG\HMG_grenade",1,1,300};
				soundBegin[] = {"begin1",1};
				weaponSoundEffect = "DefaultRifle";
			};
		};
		class Burst2: Burst1
		{
			reloadTime = 0.25;
			autoFire = "true";
			displayName = "C-40F/2.5S";
			burst = 10;
			multiplier = 4;
		};
		class Burst3: Burst1
		{
			reloadTime = 0.5;
			autoFire = "true";
			displayName = "D-40F/5S";
			burst = 10;
			multiplier = 4;
		};
		class Burst4: Burst1
		{
			reloadTime = 0.125;
			autoFire = "true";
			displayName = "E-80F/2.5S";
			burst = 20;
			multiplier = 4;
		};
		class Burst5: Burst1
		{
			reloadTime = 0.25;
			autoFire = "true";
			displayName = "F-80F/5S";
			burst = 20;
			multiplier = 4;
		};
		class Burst6: Burst1
		{
			reloadTime = 0.5;
			autoFire = "true";
			displayName = "G-80F/10S";
			burst = 20;
			multiplier = 4;
		};
	};
};
class CfgMagazines
{
	class 300Rnd_CMFlare_Chaff_Magazine;
	class 320Rnd_CMFlare_Chaff_Magazine: 300Rnd_CMFlare_Chaff_Magazine
	{
		count = 320;
	};
};